//
// Created by Dmytro Pylypenko on 2/8/17.
//

#ifndef FDF_FDF_H
#define FDF_FDF_H

#include <mlx.h>
#include <math.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <string.h>

#include <stdio.h>

#include "../libft/libft.h"
#include "../get_next_line/get_next_line.h"
#include "../model_map_creator/model_map_creator.h"

typedef struct s_xyz_move
{
	float 		x;
	float 		y;
	float 		z;
}				t_xyz_move;

typedef struct s_abg_rot
{
	float 		a;
	float 		b;
	float 		g;
}				t_abg_rot;

typedef struct s_scene
{
	void		*mlx;
	void		*win;
	t_fdf_point	*struct_map;
	t_fdf_point	*proj_map;
	t_abg_rot	*angle;
	int 		zoom;
	float		z_zoom;
	t_xyz_move	*move;

	void		*img;
	int 		endians;
	int 		bits;
	int 		size_line;
	int 		*arr;
}				t_scene;

#include "../projection_map_creator/projection_map_creator.h"
#include "../model_drawer/model_drawer.h"


#endif //FDF_FDF_H
