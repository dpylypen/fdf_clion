//
// Created by Dmytro Pylypenko on 2/17/17.
//

#include "../main/fdf.h"

int	ft_isspace(int symb)
{
	if (symb == ' ' || symb == '\t')
		return (1);
	return (0);
}

int            get_int(char ch)
{
	int i;

	ch = (char)ft_tolower(ch);
	if (ch > 96)
		i = ch - 97 + 10;
	else
		i = ch - 48;
	if (i > 15 || i < 0)
		return (-1);
	return (i);
}

int		hex_reader(char **str)
{
	int ret_val;

	(*str) += 2;
	ret_val = 0;
	while (!ft_isspace(**str) && **str)
	{
		ret_val *= 16;
		ret_val += get_int(**str);
		(*str)++;
	}
	return (ret_val);
}