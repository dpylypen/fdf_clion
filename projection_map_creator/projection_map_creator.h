//
// Created by Dmytro Pylypenko on 2/15/17.
//

#ifndef FDF_PROJECTOR_H
#define FDF_PROJECTOR_H

#include "../main/fdf.h"

//
//typedef struct s_xyz_move
//{
//	float 		x;
//	float 		y;
//	float 		z;
//}				t_xyz_move;
//
//typedef struct s_abg_rot
//{
//	float 		a;
//	float 		b;
//	float 		g;
//}				t_abg_rot;
t_fdf_point *projection_map_creator(t_fdf_point *struct_map, t_scene scene);//t_abg_rot angle, float zoom, t_xyz_move move);
void abg_rotator(t_fdf_point *struct_map, t_fdf_point *proj_map, t_abg_rot angle);
void zoom_scaler(t_fdf_point *proj_map, float zoom);
void xyz_mover(t_fdf_point *proj_map, t_xyz_move move);
#endif //FDF_PROJECTOR_H
