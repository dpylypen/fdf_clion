//
// Created by Dmytro Pylypenko on 2/18/17.
//

#include "../main/fdf.h"

void abg_rotator(t_fdf_point *struct_map, t_fdf_point *proj_map, t_abg_rot angle)
{
	float x;
	float y;
	float z;

	x = struct_map->x;
	y = struct_map->y;
	z = struct_map->z;
	proj_map->x = (x * (cosf(angle.b * 3.14f / 180.0f) * cosf(angle.g * 3.14f / 180.0f)))
				  + (y * cosf(angle.b * 3.14f / 180.0f) * -sinf(angle.g * 3.14f / 180.0f))
				  + (z * -sinf(angle.b * 3.14f / 180.0f));
	proj_map->y = (x * ((sinf(angle.a * 3.14f / 180.0f) * sinf(angle.b * 3.14f / 180.0f) * cosf(angle.g * 3.14f / 180.0f))
						+ (cosf(angle.a * 3.14f / 180.0f) * sinf(angle.g * 3.14f / 180.0f))))
				  + (y * ((sinf(angle.a * 3.14f / 180.0f) * sinf(angle.b * 3.14f / 180.0f) * -sinf(angle.g * 3.14f / 180.0f))
						  + (cosf(angle.a * 3.14f / 180.0f) * cosf(angle.g * 3.14f / 180.0f))))
				  + (z * sinf(angle.a * 3.14f / 180.0f) * cosf(angle.b * 3.14f / 180.0f));
}