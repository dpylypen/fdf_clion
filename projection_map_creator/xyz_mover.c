//
// Created by Dmytro Pylypenko on 2/17/17.
//

#include "../main/fdf.h"

void xyz_mover(t_fdf_point *proj_map, t_xyz_move move)
{
	proj_map->x = proj_map->x + move.x;
	proj_map->y = proj_map->y + move.y;
	proj_map->z = proj_map->z + move.z;
}