//
// Created by Dmytro Pylypenko on 2/17/17.
//

#include "../main/fdf.h"

void zoom_scaler(t_fdf_point *proj_map, float zoom)
{
	proj_map->x = proj_map->x * zoom;
	proj_map->y = proj_map->y * zoom;
	proj_map->z = proj_map->z * zoom;
}
